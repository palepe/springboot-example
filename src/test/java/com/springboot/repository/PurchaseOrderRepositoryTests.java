package com.springboot.repository;

import com.springboot.model.Product;
import com.springboot.model.PurchaseOrder;
import com.springboot.utils.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
@EnableJpaAuditing
public class PurchaseOrderRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PurchaseOrderRepository repository;


    @Test
    public void findAllWithinTimeIntervalTest() {
        // given
        final PurchaseOrder order = new PurchaseOrder("email@order", new LinkedList<>());
        entityManager.persist(order);
        entityManager.flush();

        // when
        final List<PurchaseOrder> found = repository.findAllWithinTimeInterval(DateUtils.getYesterday(), DateUtils.getTomorrow());

        // then
        assertThat(found.get(0).getBuyerEmailAddress())
                .isEqualTo(order.getBuyerEmailAddress());
    }

    @Test
    public void findAllWithinTimeIntervalTest_no_orders() {
        // given
        final PurchaseOrder order = new PurchaseOrder("email@order", new LinkedList<>());
        entityManager.persist(order);
        entityManager.flush();

        // when
        final List<PurchaseOrder> found = repository.findAllWithinTimeInterval(DateUtils.getYesterday(), DateUtils.getYesterday());

        // then
        assertTrue(found.isEmpty());
    }

    /**
     * Test that purchase order price does not change even though the product, that is part of the order, price changes
     */
    @Test
    public void purchaseOrderPriceStaysConstant() {
        final long initialPrice = 5;

        // given
        final Product product = new Product("email", initialPrice);
        entityManager.persist(product);
        final List<Product> products = new LinkedList<>();
        products.add(product);
        final PurchaseOrder order = new PurchaseOrder("email@order", products);
        entityManager.persist(order);
        entityManager.flush();

        // when
        product.setPrice(10);
        entityManager.merge(product);

        // then
        assertEquals((long)initialPrice, (long)order.getTotalValue());
    }

    @Test
    public void twoOrdersShareSameProduct() {

        // given
        final Product product = new Product("email", 5);
        entityManager.persist(product);
        final List<Product> products = new LinkedList<>();
        products.add(product);
        final PurchaseOrder order1 = new PurchaseOrder("email@order", products);
        entityManager.persist(order1);
        entityManager.flush();

        // when
        final PurchaseOrder order2 = new PurchaseOrder("email@order", products);
        entityManager.persist(order2);
        entityManager.flush();

        // then
        assertSame(order1.getProducts().get(0), order2.getProducts().get(0));
    }

    @Test
    public void creationDateSet() {

        // given
        final Product product = new Product("email", 5);
        entityManager.persist(product);
        final List<Product> products = new LinkedList<>();
        products.add(product);

        // when
        final PurchaseOrder order = new PurchaseOrder("email@order", products);
        entityManager.persist(order);
        entityManager.flush();

        // then
        assertNotNull(order.getCreationDate());
    }
}


