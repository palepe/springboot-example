/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.springboot.controllers;

import com.springboot.controllers.params.PurchaseOrderCreationParams;
import com.springboot.model.Product;
import com.springboot.model.PurchaseOrder;
import com.springboot.repository.ProductRepository;
import com.springboot.repository.PurchaseOrderRepository;
import com.springboot.utils.DateUtils;
import com.springboot.utils.JSONConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets/orders")
public class PurchaseOrderControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private PurchaseOrderRepository purchaseOrderRepository;

    private JSONConverter jsonConverter = new JSONConverter();

    @Test
    public void createPurchaseOrderTest() throws Exception {
        final Long purchaseOrderID = 99L;

        given(productRepository.findById(Mockito.any())).willAnswer(invocation -> {
            final Product p = new Product("name", 5);
            return Optional.of(p);
        });

        given(purchaseOrderRepository.save(Mockito.any())).willAnswer(invocation -> {
            final Object[] args = invocation.getArguments();
            final PurchaseOrder p = (PurchaseOrder) args[0];
            p.setId(purchaseOrderID);
            return p;
        });

        final List<Long> productIDs = new LinkedList<>();
        productIDs.add(5L);
        productIDs.add(7L);
        final PurchaseOrderCreationParams params = new PurchaseOrderCreationParams("email", productIDs);
        this.mockMvc.perform(post("/orders/create")
                .content(jsonConverter.toJson(params))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(99))
                ).andDo(document("create"));
    }

    @Test
    public void findPurchaseOrdersTest() throws Exception {
        final PurchaseOrder purchaseOrder = new PurchaseOrder("email", new LinkedList<>());
        final List<PurchaseOrder> purchaseOrders = new LinkedList<>();
        purchaseOrders.add(purchaseOrder);

        final Date startDate = DateUtils.getYesterday();
        final Date endDate = DateUtils.getTomorrow();

        given(purchaseOrderRepository.findAllWithinTimeInterval(any(), any())).willAnswer(invocation -> {
            return purchaseOrders;
        });

        this.mockMvc.perform(get("/orders/find")
                .param("startDate", DateUtils.formatDate(startDate))
                .param("endDate", DateUtils.formatDate(startDate))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].buyerEmailAddress", is("email"))
                ).andDo(document("find"));
    }
}
