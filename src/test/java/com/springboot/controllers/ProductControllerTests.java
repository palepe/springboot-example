/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.springboot.controllers;

import com.springboot.model.Product;
import com.springboot.repository.ProductRepository;
import com.springboot.utils.JSONConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets/products")
public class ProductControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    private JSONConverter jsonConverter = new JSONConverter();

    @Test
    public void createProductTest() throws Exception {

        final Long productID = 99L;
        given(productRepository.save(Mockito.any())).willAnswer(invocation -> {
            final Object[] args = invocation.getArguments();
            final Product p = (Product) args[0];
            p.setId(productID);
            return p;
        });

        final Product product = new Product("name", 5);
        this.mockMvc.perform(post("/products/create")
                .content(jsonConverter.toJson(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(99))
                ).andDo(document("create"));
    }

    @Test
    public void updateProductTest() throws Exception {
        final Long productID = 99L;
        final Product product = new Product("name", 5);
        product.setId(productID);

        given(productRepository.existsById(Mockito.any())).willAnswer(invocation -> {
            return true;
        });

        given(productRepository.save(Mockito.any())).willAnswer(invocation -> {
            return invocation.getArguments()[0];
        });

        this.mockMvc.perform(post("/products/update")
                .content(jsonConverter.toJson(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(99))
        ).andDo(document("update"));
    }

    @Test
    public void getProductsTest() throws Exception {
        final Long productID = 99L;
        final Product product = new Product("name", 5);
        product.setId(productID);

        final List<Product> products = new LinkedList<>();
        products.add(product);

        given(productRepository.findAll()).willAnswer(invocation -> {
            return products;
        });

        this.mockMvc.perform(get("/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(99)))
                .andExpect(jsonPath("$[0].name", is("name")))
                .andExpect(jsonPath("$[0].price", is(5))
                ).andDo(document("all"));
    }

    @Test
    public void getProductTest() throws Exception {
        final Long productID = 99L;
        final Product product = new Product("name", 5);
        product.setId(productID);

        final List<Product> products = new LinkedList<>();
        products.add(product);

        given(productRepository.findById(productID)).willAnswer(invocation -> {
            return Optional.of(product);
        });

        this.mockMvc.perform(get("/products/" + productID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(99)))
                .andExpect(jsonPath("$.name", is("name")))
                .andExpect(jsonPath("$.price", is(5))
                ).andDo(document("byID"));
    }

}
