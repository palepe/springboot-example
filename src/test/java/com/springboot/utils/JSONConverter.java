package com.springboot.utils;

import com.google.gson.Gson;

public class JSONConverter {

    private Gson gson = new Gson();

    public String toJson(Object product) {
        return gson.toJson(product);
    }
}
