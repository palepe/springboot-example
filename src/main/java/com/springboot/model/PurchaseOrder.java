package com.springboot.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToMany(
            cascade = CascadeType.REFRESH
    )
    private List<Product> products;
    private Long totalValue;
    private String buyerEmailAddress;

    @CreatedDate
    private Date creationDate;

    public PurchaseOrder() {}

    public PurchaseOrder(final String buyerEmailAddres, final List<Product> products) {
        this.buyerEmailAddress = buyerEmailAddres;
        this.products = products;
        this.totalValue = products != null ? products.stream().mapToLong(p -> p.getPrice()).sum() : 0;
    }

    public Long getId() {
        return id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public Long getTotalValue() {
        return totalValue;
    }

    public String getBuyerEmailAddress() {
        return buyerEmailAddress;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
