package com.springboot.repository;

import com.springboot.model.PurchaseOrder;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long> {

    @Query("select a from PurchaseOrder a where a.creationDate >= :startDate and a.creationDate <= :endDate")
    List<PurchaseOrder> findAllWithinTimeInterval (
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate);
}