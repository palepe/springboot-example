package com.springboot.controllers;

import com.springboot.model.Product;
import com.springboot.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Long createProduct(@RequestBody Product product) {
        if (product.getId() != null) {
            throw new RuntimeException("The product is already in the database.");
        }
        final Product savedProduct = productRepository.save(product);
        return savedProduct.getId();
    }

    @RequestMapping("")
    public @ResponseBody Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    /**
     * Get product by ID
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public @ResponseBody Product getProduct(@PathVariable("id") Long id) {
        final Optional<Product> optionalProduct = productRepository.findById(id);
        if (!optionalProduct.isPresent()) {
            throw new RuntimeException("Product not found. ID=" + id);
        }
        return optionalProduct.get();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Long updateProduct(@RequestBody Product product) {
        if (!productRepository.existsById(product.getId())) {
            throw new RuntimeException("The product is not in the database.");
        }
        final Product savedProduct = productRepository.save(product);
        return savedProduct.getId();
    }
}
