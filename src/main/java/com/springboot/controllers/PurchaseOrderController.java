package com.springboot.controllers;

import com.springboot.controllers.params.PurchaseOrderCreationParams;
import com.springboot.model.Product;
import com.springboot.model.PurchaseOrder;
import com.springboot.repository.ProductRepository;
import com.springboot.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/orders")
public class PurchaseOrderController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Long placeOrder(@RequestBody PurchaseOrderCreationParams params) {

        final List<Product> products = new LinkedList<>();
        for (final Long id : params.getProductIDs()) {
            final Optional<Product> optionalProduct = productRepository.findById(id);
            if (!optionalProduct.isPresent()) {
                throw new RuntimeException("Product not found. ID=" + id);
            }
            products.add(optionalProduct.get());
        }

        final PurchaseOrder savedPurchaseOrder = purchaseOrderRepository.save(new PurchaseOrder(params.getEmail(), products));
        return savedPurchaseOrder.getId();
    }

    @RequestMapping("")
    public @ResponseBody Iterable<PurchaseOrder> getProducts() {
        return purchaseOrderRepository.findAll();
    }

    @RequestMapping("/find")
    public @ResponseBody
    Iterable<PurchaseOrder> findProducts(@RequestParam(value="startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(value="endDate") Date endDate) {
        return purchaseOrderRepository.findAllWithinTimeInterval(startDate, endDate);
    }
}
