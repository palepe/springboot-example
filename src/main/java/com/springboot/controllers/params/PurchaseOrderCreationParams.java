package com.springboot.controllers.params;

import java.util.List;

public class PurchaseOrderCreationParams {
    private final String email;
    private final List<Long> productIDs;

    public PurchaseOrderCreationParams(String email, List<Long> productIDs) {
        this.email = email;
        this.productIDs = productIDs;
    }

    public String getEmail() {
        return email;
    }

    public List<Long> getProductIDs() {
        return productIDs;
    }
}
