# springboot-example

Build: mvn clean verify

Run: docker run -v /tmp:/tmp -p 8080:8080 docker-image/example:latest

Security: I would choose https://spring.io/projects/spring-security, because it is well integrated with SpringBoot

Redundancy: not sure if I understand the question.

Description


A tiny REST / JSON web service in Java using Spring Boot (RestController) with an API that
supports basic products CRUD:

● Create a new product

● Get a list of all products

● Update a product

The API also supports:

● Placing an order

● Retrieving all orders within a given time period

A product has a name and some representation of its price.
Each order is recorded and have a list of products. It also has the buyer’s e-mail, and the
time the order was placed. The total value of the order is always calculated, based on the prices
of the products in it.
It is possible to change the product’s price, but this does not affect the total value of orders which
have already been placed.


Implementation details:

● Unit tests.

● Documented REST-API.

● H2 storage solution.

● Docker image is available.

